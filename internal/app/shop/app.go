package shop

import (
	"context"

	"errors"
	"fmt"
	"github.com/gorilla/mux"
	"gitlab.com/d.koshlyakov/clean_architecture/internal/app/shop/api"
	"gitlab.com/d.koshlyakov/clean_architecture/internal/app/shop/config"
	"gitlab.com/d.koshlyakov/clean_architecture/internal/app/shop/repository"
	"gitlab.com/d.koshlyakov/clean_architecture/internal/app/shop/repository/products"
	"gitlab.com/d.koshlyakov/clean_architecture/internal/app/shop/service"
	"gorm.io/gorm"
	"log"
	"net/http"
)

type App struct {
	server  *api.Server
	service *service.Service
	gorm    *gorm.DB
	srv     *http.Server
	cfg     *config.Config
}

func NewApp() *App {
	return &App{}
}

func (a *App) Init(ctx context.Context, cfg *config.Config) error {
	//инициализация grpc, http, роутинг, адаптеров, репозиториев, кафка, коннекторов к другим микросервисам,
	a.cfg = cfg
	a.gorm = repository.NewGorm(a.cfg.Database.DSN)
	productRepo := products.NewRepository(a.gorm)

	a.service = service.NewService(productRepo)

	a.server = api.NewServer(a.service)

	a.newHttpServer()

	return nil
}

func (a *App) newHttpServer() {
	router := mux.NewRouter()
	router.HandleFunc("/api/v1/product/{id}", a.server.GetProduct)
	router.HandleFunc("/api/v1/products", a.server.GetProducts)

	a.srv = &http.Server{
		Handler: router,
		Addr:    fmt.Sprintf(":%s", a.cfg.HTTP.PORT),
	}
}

func (a *App) Start(ctx context.Context) error {
	//старт серверов grpc, http

	go func() {
		if err := a.srv.ListenAndServe(); err != nil && !errors.Is(err, http.ErrServerClosed) {
			log.Fatalf("Could not listen on port :%s: %s\n", a.cfg.HTTP.PORT, err.Error())
		}
	}()

	log.Printf(
		"Serving shop start at port :%s\n",
		a.cfg.HTTP.PORT,
	)

	return nil
}

func (a *App) Stop(ctx context.Context) error {
	<-ctx.Done()

	done := make(chan bool)
	log.Printf("Server is shutting down...")

	// остановка приложения, gracefully shutdown
	go func() {
		if err := a.srv.Shutdown(context.Background()); err != nil {
			log.Fatal("Could not gracefully shutdown the server: ", err.Error())
		}

		log.Printf("Server stopped")
		close(done)
	}()

	<-done
	return nil
}
