package service

import (
	"context"
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/d.koshlyakov/clean_architecture/internal/app/shop/service/mocks"
	"gitlab.com/d.koshlyakov/clean_architecture/pkg/models"
)

func TestGetProduct(t *testing.T) {
	//arrange
	repo := mocks.ProductsRepositoryMock{
		Product: &models.Product{
			ID: 4,
		},
	}

	service := NewService(repo)

	want := &models.Product{
		ID: 4,
	}

	//act
	got, err := service.Get(context.Background(), 4)

	//assert
	assert.Nil(t, err)
	assert.Equal(t, want, got)
}

func TestGetProducts(t *testing.T) {
	//arrange
	repo := mocks.ProductsRepositoryMock{
		Products: models.Products{
			{
				ID: 4,
			},
			{
				ID: 6,
			},
		},
	}

	service := NewService(repo)

	want := models.Products{
		{
			ID: 4,
		},
		{
			ID: 6,
		},
	}

	//act
	got, err := service.List(context.Background())

	//assert
	assert.Nil(t, err)
	assert.Equal(t, want, got)
}
