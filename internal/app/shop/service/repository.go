package service

import (
	"context"
	"gitlab.com/d.koshlyakov/clean_architecture/pkg/models"
)

type ProductsRepository interface {
	Get(ctx context.Context, id int32) (*models.Product, error)
	List(ctx context.Context) (models.Products, error)
}
