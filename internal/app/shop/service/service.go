package service

import (
	"context"

	"gitlab.com/d.koshlyakov/clean_architecture/pkg/models"
)

type Service struct {
	productRepo ProductsRepository
}

func NewService(
	productRepo ProductsRepository,
) *Service {
	return &Service{
		productRepo: productRepo,
	}
}

func (s *Service) Get(ctx context.Context, id int32) (*models.Product, error) {
	return s.productRepo.Get(ctx, id)
}

func (s *Service) List(ctx context.Context) (models.Products, error) {
	return s.productRepo.List(ctx)
}
