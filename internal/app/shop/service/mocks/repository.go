package mocks

import (
	"context"
	"gitlab.com/d.koshlyakov/clean_architecture/pkg/models"
)

type ProductsRepositoryMock struct {
	Product  *models.Product
	Products models.Products
}

func (p ProductsRepositoryMock) Get(ctx context.Context, id int32) (*models.Product, error) {
	return p.Product, nil
}

func (p ProductsRepositoryMock) List(ctx context.Context) (models.Products, error) {
	return p.Products, nil
}
