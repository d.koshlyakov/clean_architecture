package api

import (
	"encoding/json"
	"fmt"
	"github.com/gorilla/mux"
	"gitlab.com/d.koshlyakov/clean_architecture/internal/app/shop/service"
	"net/http"
	"strconv"
)

type Server struct {
	productService *service.Service
}

func NewServer(productService *service.Service) *Server {
	return &Server{
		productService: productService,
	}
}

func (s *Server) GetProduct(w http.ResponseWriter, req *http.Request) {
	ctx := req.Context()

	vars := mux.Vars(req)
	idStr, ok := vars["id"]
	if !ok {
		fmt.Println("id is missing in parameters")
	}

	id, err := strconv.Atoi(idStr)

	product, err := s.productService.Get(ctx, int32(id))
	if err != nil {
		return
	}

	w.Header().Set("Content-Type", "application/json; charset=utf-8")
	w.WriteHeader(http.StatusOK)

	if err := json.NewEncoder(w).Encode(product); err != nil {
		return
	}
}

func (s *Server) GetProducts(w http.ResponseWriter, req *http.Request) {
	ctx := req.Context()
	products, err := s.productService.List(ctx)
	if err != nil {
		return
	}

	w.Header().Set("Content-Type", "application/json; charset=utf-8")
	w.WriteHeader(http.StatusOK)

	if err := json.NewEncoder(w).Encode(products); err != nil {
		return
	}
}
