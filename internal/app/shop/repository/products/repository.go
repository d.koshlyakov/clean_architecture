package products

import (
	"context"
	"gitlab.com/d.koshlyakov/clean_architecture/pkg/models"
	"gorm.io/gorm"
)

type Repository struct {
	db *gorm.DB
}

func NewRepository(db *gorm.DB) *Repository {
	return &Repository{
		db: db,
	}
}

func (r Repository) Get(ctx context.Context, id int32) (*models.Product, error) {
	var product Product

	return toModelsProduct(product), nil
}

func (r Repository) List(ctx context.Context) (models.Products, error) {
	var products Products
	//sql реквест в бд
	//скан данных в структуру &products

	return toModelsProducts(products), nil
}
