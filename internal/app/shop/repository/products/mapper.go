package products

import "gitlab.com/d.koshlyakov/clean_architecture/pkg/models"

func toModelsProduct(product Product) *models.Product {
	return &models.Product{
		ID: product.ID.Int32,
	}
}

func toModelsProducts(products Products) models.Products {
	res := make(models.Products, 0, len(products))

	for _, product := range products {
		res = append(res, toModelsProduct(product))
	}
	return res
}
